var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyParser = require('body-parser');
var express = require('express');

var cors = require('cors');
var app = express();
app.use(cors());

var indexRouter = require('./routes/index');
var fighterRouter = require('./routes/fighter');

app.use(bodyParser.urlencoded({ extended : false }));
app.use(bodyParser.json());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use('/', indexRouter);
app.use('/fighter', fighterRouter);

module.exports = app;
