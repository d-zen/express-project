var express = require('express');
var bodyParser = require('body-parser');
var router = express.Router();
var app = express();
var fs = require('fs');
var {fighters} = require('../data/default_fighters');

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

const {saveName, createFighter} = require('../services/fighter.servise');
var path = 'data/saved_fighters.json';
var deletedFighters = [];

var actualFighters = null;

setUsers = (function() {
  if (fs.existsSync(path)) {
    fs.readFile(path, function(err, buf) {
      if (buf) {
        actualFighters = JSON.parse(buf.toString());
      }
    });
  } else actualFighters = fighters;
})();

// endpoints
router.get('/', function (req, res) {
  res.status(200).send(actualFighters)
});

router.get('/:id', function (req, res) {
  const result = actualFighters.find(fighter => fighter.id === req.params.id);
  if (result) {
    res.send(result);
  } else {
    res.status(404).send(`Fighter with id:${req.params.id} not found`);
  }
});

router.post('/', function (req, res) {
  if (req.body) {
    const data = req.body;
    let newUser = createFighter(data);
    // ToDo: check if newUser instanceof Fighter
    let largest = 0;
    actualFighters.forEach(fighter => {
      if (fighter.id > largest) {
        largest = fighter.id
      };
      largest++
    });
    newUser.id = largest;
    fs.writeFile(path, actualFighters, (err) => {
      if (err) console.log(err);
      console.log("File successfully updated.");
    });
    actualFighters.push(newUser);
    res.status(201).send('Fighter created, and added to others.')
  } else {
    res.status(400).send('Error. Figure out what kind of.');
  }
});

router.put('/:id', function (req, res) {
  if (req.body && req.params.id) {
    if (null == req.body || 'object' != typeof req.body) {
      res.status(400).send('Invalid data.');
    } else {
      let data = req.body;
      let params = req.params;
      let result = actualFighters.find(fighter => fighter.id == params.id);

      if(!result) {
        res.status(404).send(`No user found with id: ${params.id}`);
        return
      }

      for (let prop in data) {
        if (data.hasOwnProperty(prop)) result[prop] = data[prop];
      }
      res.status(202).send(`Fighter ${data.name} updated successfully.`);
    }
  } else {
    res.status(404).send('No data to work with.');
  }
});

router.delete('/:id', function (req, res) {
  if ( req.params && req.params.id) {
    let index = fighters.indexOf(
      actualFighters.find(fighter => fighter.id === req.params.id)
    );
    if (index !== -1) {
      deletedFighters.push(actualFighters[index]);
      actualFighters.splice(index, 1);
      res.status(200).send(`Fighter w/id:${req.params.id} deleted successfully.`);
    } else {
      res.status(404).send(`Fighter w/id:${req.params.id} not found.`);
    }
  } else {
    res.status(400).send('No data to work with.');
  }
});

module.exports = router;
