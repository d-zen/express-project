# Express backend for Street Fighter APP

This is API for, [Street Fighter](https://bitbucket.org/d-zen/street-fighter).
Clone or download it, install all dependencies using `npm install` and run Express server with `npm start`.

When ready, clone [Street Fighter](https://bitbucket.org/d-zen/street-fighter),  install dependencies `npm install` and
start Angular local server `ng serve`.
