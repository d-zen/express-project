const { saveData } = require('../repositories/fighter.repository');

const getName = (fighter) => {
  if (fighter) {
    return fighter.name;
  } else {
    return null;
  }
};

const saveName = (fighter) => {
  if (fighter) {
    return saveData(fighter.name);
  } else {
    return false;
  }
};


const createFighter = (data) => {
  // todo: use constructor/strict way to produce new fighter
  if (null == data || "object" != typeof data) return data;
  let fighter = {};
  for (let attr in data) {
    if (data.hasOwnProperty(attr)) fighter[attr] = data[attr];
  }
  return fighter;
};

module.exports = { getName, saveName, createFighter: createFighter };
