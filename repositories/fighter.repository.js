const saveData = (data) => {
  if (data) {
    // code to save data to DataBase
    console.log(`${data} saved successfully!`);
    return true;
  } else {
    return false;
  }
};

module.exports = { saveData };
