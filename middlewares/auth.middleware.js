const isAuthorized = (req, res, next) => {
  if (
      req &&
      req.headers &&
      req.headers.authorization &&
      req.headers.authorization === 'admin'
    ) {
        next();
      } else {
      // console.log(`No headers in ${req} detected.`);
      res.status(401).send('Admins permission only!');
      }
};

module.exports = {isAuthorized};
